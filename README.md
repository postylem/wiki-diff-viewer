Making a Wikipedia typo corpus.

```
./
├── *.py                    - various example python scripts
└── viewer/                 - a javascript viewer app
    ├── index.html          - <-- use for playing with data
    ├── js/
    ├── css/
    ⋮
```

in ipython/notebook
```python
%load_ext autoreload
%autoreload all -p
from pywikitypo import *
# Compare two revids.
s = WikiTypos()
s.compare(1175947655,1175947720)
```

### Basic idea

1.  On a given page, identify potential *typo-fixing revisions* by
    1.  getting page revision history, and filtering by revision comment (e.g. regex `/spelling|typo/i`)
    2.  examining result to refine selection, and identify specific hunks that are of interest. Let such be identified by a pair **(fixRev.id,** **hunk)**
2.  For a given (fixRev.id, hunk) pair, use WikiBlame or the like to find revision that *introduced* the typo (**typoRev**). Create typo datapoint as
    1.  surrounding context (from fixRev), cleaned up (using )
    2.  typo hunk and its fix
    3.  longevity `longevity = fixRev.timestamp - typoRev.timestamp` (to track difficulty of spotting it)