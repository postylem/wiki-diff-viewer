from typing import Optional


class ChangeRange:
    """
    Simple wrapper for dict representing byte change.
    """

    def __init__(self, d) -> None:
        """
        Args:
            d (dict): a highlightRange dict with keys 'start', 'length' and 'type'
        """
        self._data = d
        self.start = d["start"]
        self.length = d["length"]
        self.end = self.start + self.length
        self.change = self._readable_type(d["type"])

    def _readable_type(self, t):
        return "del" if t == 1 else "add" if t == 0 else None

    def __repr__(self) -> str:
        """String representation of ChangeRange."""
        return f"{self.__class__.__name__}(start: {self.start}, length: {self.length}, change: {self.change})"

    def __str__(self) -> str:
        """Printable representation of ChangeRange."""
        return str(self._data)


class HighlightRanges:
    """
    Wrapper for the list of ranges (dicts) describing a comparison,
    found in ._data["diff"]["highlightRanges"] of a Comparison object.
    """

    def __init__(
        self, highlightRanges: list[dict], text_length: Optional[int] = None
    ) -> None:
        if not type(highlightRanges) is list:
            raise TypeError(f"Not list. Instead, type is {type(highlightRanges)}")
        self._data = highlightRanges
        self.text_length = text_length

    def additions(self) -> list[ChangeRange]:
        return list(ChangeRange(r) for r in self._data if r["type"] == 0)

    def deletions(self) -> list[ChangeRange]:
        return list(ChangeRange(r) for r in self._data if r["type"] == 1)

    def unchanged(self) -> list[ChangeRange]:
        return list(ChangeRange(r) for r in self.get_unhighlighted())

    def get_unhighlighted(self) -> list[dict]:
        """Get complement of highlighted ranges

        Args:
            highlightRanges (list[dict]): the list of changed ranges
            (as from a Comparison object's ['diff']['highlightRanges']).

        Returns:
            list[dict]: a list of the complement of the hightlighted ranges
        """
        highlightRanges = sorted(self._data, key=lambda r: r["start"], reverse=True)
        # Initialize start (of unchanged region) at 0
        ranges, start = [], 0
        while highlightRanges:
            h = highlightRanges.pop()
            if (end := h["start"]) > start:
                ranges.append(
                    {
                        "start": start,
                        "length": end - start,
                        "type": None,
                    }
                )
            start = h["start"] + h["length"]
        # If we've exhausted the list of highlighted ranges, the rest is unchanged
        if self.text_length and self.text_length > start:
            ranges.append(
                {
                    "start": start,
                    "length": self.text_length - start,
                    "type": None,
                }
            )
        return ranges

    def get_ChangeRanges(self) -> list[ChangeRange]:
        return sorted(
            self.unchanged() + self.deletions() + self.additions(),
            key=lambda r: r.start,
        )
    
    def __repr__(self) -> str:
        """String representation of HighlightRanges."""
        return f"{self.__class__.__name__}({self._data}, text_length={self.text_length})"

    def __str__(self) -> str:
        """Printable representation of HighlightRanges."""
        return str(self._data)


class Comparison:
    """
    Wrapper for output of REST API's [compare](https://www.mediawiki.org/wiki/API:REST_API/Reference#Compare_revisions) route.
    """

    data: dict

    def __init__(self, data: dict, 
                 revision_metadata: dict,
                 metadata: dict):
        self._data = data
        self.revision_metadata = revision_metadata
        self.from_revid = data["from"]["id"]
        self.to_revid = data["to"]["id"]
        self.metadata = metadata
        self.nontrivial_diffs = self.get_nontrivial_diffs()

    def __getitem__(self, item):
        return self._data[item]

    def __iter__(self):
        """Provide Comparison data as iterator."""
        return iter(self._data["diff"])

    def __repr__(self) -> str:
        """String representation of Comparison."""
        return f"{self.__class__.__name__}({self._data})"

    def __str__(self) -> str:
        """Printable representation of Comparison data."""
        return str(self._data)
    
    def get_nontrivial_diffs(self) -> list[dict]:
        diffs = []
        for d in self._data["diff"]:
            newdict = d.copy()
            if 'highlightRanges' in d:
                t = d['text'].encode()
                hr = HighlightRanges(d['highlightRanges'], text_length=len(t))
                newdict['highlightRanges'] = hr
                diffs.append(newdict)
        return diffs

