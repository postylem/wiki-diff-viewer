import os
import re
import pywikibot
from diskcache import Cache
from wikiwho_wrapper import WikiWho
from typing import Iterable, Iterator, Any
from pywikibot.page._page import Page as pwb_Page
from pywikibot.page._revision import Revision as pwb_Revision

from .utils import *
from .compare import *

HEADERS = {"User-Agent": "wikitypos/0.1 (postylem)"}

WW_CACHE_DIR = "WIKIDATA/cache"
WW_CACHE = Cache(WW_CACHE_DIR)
WW_DOMAIN = "wikiwho-api.wmcloud.org"
WW_VERSION = "v1.0.0-beta"


# memoize the wikiwho calls
@WW_CACHE.memoize()
def _get_ww_revisions_and_history(language, pageid):
    print(f"getting revisions and history for pageid {pageid}...")
    ww = WikiWho(lng=language, domain=WW_DOMAIN, version=WW_VERSION)
    return ww.api.rev_ids_of_article(pageid)


@WW_CACHE.memoize()
def _get_ww_specific_rev_content_by_rev_id(language, revid):
    print(f"getting dv for {revid = }")
    ww = WikiWho(lng=language, domain=WW_DOMAIN, version=WW_VERSION)
    return ww.dv.specific_rev_content_by_rev_id(revid)


def filter_revisions_by_comment_regex(
    revs: Iterable[pwb_Revision], regex: str = r"(?i)spelling|typo|grammar"
):
    return filter(lambda r: bool(re.search(regex, r.comment)), revs)


def format_page_filename(pageid, safe_title):
    return "pageid" + str(pageid) + "_" + safe_title


def find_already_dumped(data_subdir):
    """Assumes filenames are made with format_page_filename"""
    pageids = {}
    if os.path.exists(data_subdir):
        filenames = os.listdir(data_subdir)
        for f in filenames:
            split_name = re.split(r"^pageid([0-9]*)_", f, maxsplit=1)
            if len(split_name) > 1:
                pageid = str(split_name[1])
                pageids |= {pageid: f}
    return pageids


# def ranges_to_all_additions_deletions(highlightRanges: list[dict]) -> dict:
#     """shouldn't have to use this"""
#     additions, deletions = [], []
#     for r in sorted(highlightRanges, key=lambda r: r["start"]):
#         (additions if r["type"] == 0 else deletions).extend(
#             range(r["start"], r["start"] + r["length"])
#         )
#     return {"all_additions": additions, "all_deletions": deletions}

# class RevisionComparison(NamedTuple):
#     revision: pwb_Revision
#     comparison: Comparison


class WikiTypos:
    def __init__(
        self,
        language: str = "en",
        domain: str = "wikipedia",
        regex: str = r"(?i)spelling|typo",
        data_to: str = "WIKIDATA/default",  # legible readable data (json)
    ) -> None:
        self.regex = regex
        self.language = language
        self.domain = domain
        self.site = pywikibot.Site(language, domain)
        self.headers = HEADERS
        print(self.site, "with regex =", regex)
        self.data_to = data_to
        self.ww = WikiWho(lng=self.language, domain=WW_DOMAIN, version=WW_VERSION)

    def get_metadata(
        self, include_regex: bool = True, include_headers: bool = False
    ) -> dict:
        metadata = {
            "language": self.language,
            "domain": self.domain,
            "data_to": self.data_to,
        }
        if include_regex:
            metadata |= {"regex": self.regex}
        if include_headers:
            metadata |= {"headers": self.headers}
        return metadata

    def page(self, page_title: str) -> pwb_Page:
        return pywikibot.Page(self.site, page_title)

    def download_comparisons(
        self, revisions_metadata: dict, comparisons_dirname="comparisons"
    ):
        comparisons_path = os.path.join(self.data_to, comparisons_dirname)

        revisions = revisions_metadata["filtered_revisions_list"]
        pageid, title = [revisions_metadata[k] for k in ["pageid", "title"]]
        filename = format_page_filename(pageid, revisions_metadata["title_as_filename"])

        already_scraped = find_already_dumped(comparisons_path)

        if str(pageid) in already_scraped:
            print(
                f"{pageid} ('{title}') [comparisons already exist at: {comparisons_path}/{already_scraped[str(pageid)]} ]."
            )
        else:
            print(
                f"{pageid} ('{title}') comparisons downloading ... to:  {comparisons_path}/{filename}"
            )
            comparisons = [self.compare_prev(r) for r in revisions]
            data = {
                "pageid": pageid,
                "title": title,
                "regex": self.regex,
                "comparisons": comparisons,
            }
            dump_json(comparisons_path, filename, data)
            info = self.get_metadata(include_regex=False) | {
                "comparisons_dirname": comparisons_dirname,
                "revisions_metadata": {
                    k: revisions_metadata[k]
                    for k in revisions_metadata
                    if k != "filtered_revisions_list"
                },
            }
            dump_metadata_dict(comparisons_path, "info", info)

    def retrieve_comparisons(self, title: str, comparisons_dirname="comparisons"):
        comparisons_path = os.path.join(self.data_to, comparisons_dirname)
        p = self.page(title)
        pageid, safe_title = p.pageid, p.title(as_filename=True)
        filename = format_page_filename(pageid, safe_title)
        return load_json(comparisons_path, filename)

    def download_filtered_revisions(
        self,
        title: str,
        filtered_revisions_dirname="filtered_revisions",
    ):
        """Downloads revisions for article, saves in filtered_revisions dir.

        Args:
            article_to_scrape (str): article title
            filtered_revisions_dirname (str, optional): directory relative to self.wikidata to save to. Defaults to "filtered_revisions".

        """
        filtered_revisions_path = os.path.join(self.data_to, filtered_revisions_dirname)

        p = self.page(title)
        pageid, safe_title = p.pageid, p.title(as_filename=True)
        filename = format_page_filename(pageid, safe_title)

        already_scraped = find_already_dumped(filtered_revisions_path)

        if str(pageid) in already_scraped:
            print(
                f"{pageid} ('{title}') [revisions already exist at: {filtered_revisions_path}/{already_scraped[str(pageid)]} ]."
            )
        else:
            print(
                f"{pageid} ('{title}') revisions downloading ... to: {filtered_revisions_path}/{filename}"
            )
            filtered_revisions_list = list(
                r._data for r in self.get_filtered_revisions(p)
            )
            data = {
                "pageid": pageid,
                "title": title,
                "title_as_filename": safe_title,
                "regex": self.regex,
                "filtered_revisions_list": filtered_revisions_list,
            }
            # dump_pkl(filtered_revisions_path, filename, data)
            dump_json(filtered_revisions_path, filename, data)
            additional_info = {
                "title": title,
                "filtered_revisions_dirname": filtered_revisions_dirname,
            }
            dump_metadata_dict(
                filtered_revisions_path, "info", self.get_metadata() | additional_info
            )

    def retrieve_revisions_metadata(
        self,
        title: str,
        filtered_revisions_dirname="filtered_revisions",
    ):
        """Retrieves from disk metadata downloaded with `download_filtered_revisions`

        Args:
            article_to_scrape (str): article title
            filtered_revisions_dirname (str, optional): directory relative to self.wikidata to read from. Defaults to "filtered_revisions".
        """
        filtered_revisions_path = os.path.join(self.data_to, filtered_revisions_dirname)

        p = self.page(title)
        pageid, safe_title = p.pageid, p.title(as_filename=True)
        filename = format_page_filename(pageid, safe_title)
        # return load_pkl(filtered_revisions_path, filename)
        return load_json(filtered_revisions_path, filename)

    def compare(self, from_rev: int, to_rev: int) -> dict[str, Any]:
        rest_url = f"https://{self.language}.{self.domain}.org/w/rest.php/v1/"
        route = "revision/" + str(from_rev) + "/compare/" + str(to_rev)
        json = fetch_json(rest_url + route, headers=self.headers)
        return json

    def get_filtered_revisions(self, page: pwb_Page) -> Iterator[pwb_Revision]:
        """Return an iterator of filtered revisions, type `pywikibot.page._revision.Revision`"""
        revs = page.revisions()
        return filter_revisions_by_comment_regex(revs, self.regex)

    # def get_filtered_revisions_list(self, page: pwb_Page) -> list:
    #     return list(self.get_filtered_revisions(page))

    def compare_prev(self, revision: pwb_Revision) -> dict:
        return self.compare(revision["parentid"], revision["revid"])

    # def get_typo_revisions_with_compare_prev(
    #     self, page: pwb_Page
    # ) -> Iterator[tuple[pwb_Revision, dict]]:
    #     return map(
    #         lambda r: RevisionComparison(r, self.compare_prev(r)),
    #         self.get_filtered_revisions(page),
    #     )

    def merge_comparisons_with_metadata(self, rev_data, comp_data):
        top_level_metadata = {
            k: comp_data[k] for k in comp_data if k != "comparisons"
        } | {k: rev_data[k] for k in rev_data if k != "filtered_revisions_list"}
        print("metadata:", top_level_metadata)
        cs, rs = comp_data["comparisons"], rev_data["filtered_revisions_list"]
        assert len(cs) == len(rs)
        return [
            Comparison(data=c, revision_metadata=r, metadata=top_level_metadata)
            for c, r in zip(cs, rs)
        ]

    def retrieve_comparisons_with_metadata(
        self,
        title: str,
        comparisons_dirname="comparisons",
        filtered_revisions_dirname="filtered_revisions",
    ):
        rev_data = self.retrieve_revisions_metadata(title, filtered_revisions_dirname)
        comp_data = self.retrieve_comparisons(title, comparisons_dirname)
        return self.merge_comparisons_with_metadata(rev_data, comp_data)

    def get_typo_lifespan(self, typo_fix_comparison: Comparison, typo_token):
        pass

    def _get_typo_ages(self, typo_fix_comparison: Comparison, typo_token):
        typo_fixed_revid = typo_fix_comparison.to_revid
        typo_final_revid = typo_fix_comparison.from_revid
        print(f"{typo_final_revid} ~> {typo_fixed_revid}")
        pageid = typo_fix_comparison.metadata["pageid"]

        revisions_and_info = _get_ww_revisions_and_history(
            self.language, pageid
        )  # memoized
        revisions = revisions_and_info["revisions"]
        print(revisions_and_info["article_title"], len(revisions), "revisions")
        with_typo_df = _get_ww_specific_rev_content_by_rev_id(
            self.language, typo_final_revid
        )
        # return revisions, with_typo_df

    # def _rest(
    #     self, typo_fix_comparison: Comparison, typo_token, with_typo_df, revisions
    # ):
    #     typo_fixed_revid = typo_fix_comparison.to_revid

        typo_locations = with_typo_df.query("token == @typo_token").index
        if len(typo_locations) > 1:
            print("More than one match for typo word...")
        print("In context:")
        window_size = 3
        for i in typo_locations:
            before = with_typo_df.loc[i - window_size : i - 1, :]
            after = with_typo_df.loc[i + 1 : i + window_size, :]
            t = with_typo_df.loc[i:i, :]["token"].values[0]
            print(i, " ".join(before["token"]) + f"┊{t}┊" + " ".join(after["token"]))
            # peek = with_typo_df.loc[i-window_size:i+window_size,:]
            # display(peek)

        selected = with_typo_df.loc[typo_locations]

        if not len(selected) == 1:
            print(selected)
            raise AssertionError(f"{len(selected)} matches found. Expected 1.")

        typo_born_revid = selected.o_rev_id.values[0]

        typo_fixed_info = list(filter(lambda r: r["id"] == typo_fixed_revid, revisions))
        if not len(typo_fixed_info) == 1:
            raise AssertionError(
                f"{len(typo_fixed_info)} matches found for single revid {typo_fixed_revid}"
            )
        typo_fixed_timestamp_str = typo_fixed_info[0]["timestamp"]
        typo_fixed_time = datetime.datetime.fromisoformat(typo_fixed_timestamp_str)
        print("typo_fixed_time  =", typo_fixed_time)

        typo_born_info = list(filter(lambda r: r["id"] == typo_born_revid, revisions))

        typo_born_timestamp_str = typo_born_info[0]["timestamp"]
        typo_born_time = datetime.datetime.fromisoformat(typo_born_timestamp_str)
        print("typo_born_time =", typo_born_time)

        typo_lifetime = typo_fixed_time - typo_born_time
        print("typo lifetime  =", str(typo_lifetime))
        return {
            "ww_token_info":selected.iloc[0,:].to_dict(),
            "born":typo_born_time,
            "fixed":typo_fixed_time,
            "lifespan":typo_lifetime
        }
