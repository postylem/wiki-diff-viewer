import os
import json
import yaml
import pickle
import requests
import datetime
import mwparserfromhell

from itertools import groupby, count
from operator import add, sub
from pywikibot.time import Timestamp

from Levenshtein import distance as lev

def levenshtein_predicate(threshold=1):
    """Make predicate for whether Levenshtein distance is (at or) below threshold"""

    def f(string1, string2):
        return lev(string1, string2) <= threshold

    return f


def strip_wikicode(string: str):
    return mwparserfromhell.parse(string).strip_code()


def group_consecutive(lst, descending=False) -> list[list]:
    """Group consecutive numbers in a list.

    Example:
    >>> group_consecutive([5, 4, 3, 1], descending=True)
    [[5, 4, 3], [1]]
    """
    pm = add if descending else sub
    return [list(g) for _, g in groupby(lst, key=lambda x, c=count(): pm(x, next(c)))]

def dump_metadata_dict(target_path, target_file, metadata_dict, extension=".txt"):
    if not os.path.exists(target_path):
        try:
            os.makedirs(target_path)
        except Exception as e:
            print(e)
            raise
    with open(os.path.join(target_path, target_file + extension), "a") as f:
        f.write("##########" + str(datetime.datetime.now()) + "\n")
        f.write(yaml.dump(metadata_dict))
        f.write("##########\n")

def dump_pkl(target_path, target_file, object, extension=".pkl"):
    if not os.path.exists(target_path):
        try:
            os.makedirs(target_path)
        except Exception as e:
            print(e)
            raise
    with open(os.path.join(target_path, target_file + extension), "wb") as f:
        pickle.dump(object, f, protocol=pickle.HIGHEST_PROTOCOL)


def load_pkl(target_path, target_file, extension=".pkl"):
    target_path = os.path.join(target_path, target_file + extension)
    if not os.path.exists(target_path):
        raise ValueError(f"{target_path} does not exist")
    with open(target_path, "rb") as f:
        return pickle.load(f)


def time_default_serializer(obj):
    """
    Serializer for datetime.datetime or pywikibot.time.Timestamp objects,
    which preserves the difference, so they can be deserialized unambiguously.
    """
    if isinstance(obj, Timestamp):  # a subclass of datetime.datetime, so check first
        return {"_timestamp_isoformat": obj.isoformat()}
    if isinstance(obj, datetime.datetime):
        return {"_isoformat": obj.isoformat()}
    raise TypeError(f"Serialization failed for object {obj}")


def time_object_hook_deserializer(obj):
    """
    Deserializer object hook. Recovers datetime.datetime or pywikibot.time.Timestamp objects
    if time_default_serializer was used to serialize
    """
    _timestamp_isoformat = obj.get("_timestamp_isoformat")
    if _timestamp_isoformat is not None:
        return Timestamp.fromISOformat(_timestamp_isoformat)
    _isoformat = obj.get("_isoformat")
    if _isoformat is not None:
        return datetime.datetime.fromisoformat(_isoformat)
    return obj


def dump_json(
    target_path,
    target_basename,
    data,
    extension=".json",
    default=time_default_serializer,
):
    if not os.path.exists(target_path):
        try:
            os.makedirs(target_path)
        except Exception as e:
            print(e)
            raise
    with open(os.path.join(target_path, target_basename + extension), "w") as f:
        json.dump(data, f, default=default)


def load_json(
    target_dir,
    target_basename,
    extension=".json",
    object_hook=time_object_hook_deserializer,
):
    target_path = os.path.join(target_dir, target_basename + extension)
    if not os.path.exists(target_path):
        raise ValueError(f"{target_path} does not exist")
    with open(target_path, "r") as f:
        return json.load(f, object_hook=object_hook)


def fetch_json(url: str, headers: dict = {}) -> dict:
    response = requests.get(url, headers=headers)
    data = response.json()
    return data


def put_in_brackets_if_multiple(string: str, sep: str = "|", extend: str = " ") -> str:
    """Printing helper: put in characters in string spaced in brackets if multiple characters"""
    if len(string) <= 1:
        return string
    else:
        s = (
            string[0]
            + (extend * 2 + sep)
            + (extend * 2 + sep).join(string[1:])
            + extend
        )
        return "[" + s + "]"


def mark_bytes(
    string: str,
    mark_byte=lambda bytenum: bytenum % 10,
    encoding: str = "utf-8",
    echo: bool = False,
) -> str:
    """Illustrate the bytes of a string

    Args:
        string (str): input string
        mark_byte (function, optional): function for how to mark byte by num. Defaults to `lambda bytenum: bytenum%10`.
        encoding (str, optional): Defaults to 'utf-8'.
        echo (bool, optional): Prepend bytestring itself. Defaults to False.

    Returns:
        str: Bytes marked to print
    """
    output = "  "
    if echo:
        output = str(string.encode(encoding)) + "\n" + output
    c = count()
    for i, char in enumerate(string):
        # x = str(mark_char(i, char))
        bytes = char.encode(encoding)
        # marker_line += put_in_brackets_if_multiple(x*len(bytes))
        bytenums = [next(c) for _ in bytes]
        output += put_in_brackets_if_multiple(
            "".join(str(mark_byte(j)) for j in bytenums)
        )
    return output


def parse_changed_bytes_from_lists(
    deletions: list[int], additions: list[int], descending=False
) -> list[dict]:
    """
    Find adjacent ranges where deletion immediately precedes addition. Either may be empty.

    To be used on byte-deletions and -additions from 'highlightRanges' returned by the MediaWiki REST API.

    Input: disjoint unique lists of integers representing locations of `deletions` and `additions`.
    Returns a list of dictionaries describing each change.
    A change is a proper substitution when both are nonempty, else it is an addition or deletion.

    Args:
        deletions (list[int]): deleted bytes' locations from beginning of string
        additions (list[int]): added bytes' location from beginning of string
        descending (bool, optional): whether to return bytes in descending order. Defaults to False.

    Returns:
        list[dict]: list of dicts comprising 'del' and 'add' keys for each revision.

    Example:
    >>> parse_changed_bytes_from_lists([1,2,8], [3,4,6])
    [{'del': [1, 2], 'add': [3, 4]}, // a substitution
     {'del': [], 'add': [6]},        // an addition
     {'del': [8], 'add': []}]        // a deletion
    """
    if (
        set(deletions) & set(additions)
        or (len(set(deletions)) != len(deletions))
        or (len(set(deletions)) != len(deletions))
    ):
        raise ValueError(
            "Input lists 'deletions' and 'additions' must be disjoint lists of unique values."
        )

    # group consecutive values of deletions and additions lists
    # use descending order, so we can pop them off starting with smallest
    dg, ag = (
        group_consecutive(sorted(lst, reverse=True), descending=True)
        for lst in [deletions, additions]
    )
    changes = []

    while dg or ag:
        # Determine the 'del' and 'add' lists (both are nonempty only if they're adjacent)
        d = dg.pop() if dg and (not ag or dg[-1][-1] + 1 <= ag[-1][0]) else []
        a = ag.pop() if ag and (not dg or dg[-1][-1] + 1 >= ag[-1][0]) else []
        if not descending:
            # Reverse the lists (they're small, no problem)
            d, a = d[::-1], a[::-1]
        # Append the current change to the list of changes
        changes.append({"del": d, "add": a})

    return changes
