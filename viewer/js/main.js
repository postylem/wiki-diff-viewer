const HEADERS = {
    // 'Authorization': 'Bearer XXX',
    'Api-User-Agent': 'YYY'
}
const PROJ = 'wikipedia'
const FINISHED_MESSAGE = "(at oldest)"
const MAX_STRING_LENGTH = 200
var revisionsCount = 0
$(function () {
    ['top', 'history', 'diff', 'comparison'].forEach(label => {
        $(`#header-${label}`).on('click', function () { $(`#sec-${label}`).toggle() })
    });
    renderjson.set_icons("⯈", "⯆")
        .set_max_string_length(MAX_STRING_LENGTH)
        .set_show_to_level(0)
    $('#titleSearchInput').on('input', async function (e1) {
        let pages = await search_titles($(this).val())
        setSelectedPage(pages[0]);
        $('#autocompleteSelect').off().on('change', function (e2) {
            console.log(this.value)
            setSelectedPage(pages[this.value]);
        })
    });
    sync_all_displays();
    $('#diffURL').on('click', function () { get_diff_with_prev() });
    $('#pageTitle').on('input', function () {
        $('#olderThan').val("");
        $("#historyButton").attr("disabled", false);
    })
    $('#countEditsButton').on('click', function () { get_history_count() });
    $('#olderThan').on('input', function () {
        $("#historyButton").attr("disabled", this.value == FINISHED_MESSAGE)
    })
    $('#historyButton').on('click', function () { get_page_history() });
    $('#maxBatches').on('input', function () { $('#historyBatchOutOf').html("/" + $('#maxBatches').val()) });
    $('#historyBatchOutOf').html("/" + $('#maxBatches').val());
    $('#filteredClearButton').on('click', function () { 
        $('#history-filtered').empty() 
        revisionsCount = 0;
        display_revisionsCount(revisionsCount);
    });
    $('#unfilteredClearButton').on('click', function () {
        $('#history-unfiltered').empty();
        $("#history-unfiltered:empty").parent().hide();
    });
    $('#revidInput').on('input', function () { get_diff_with_prev() });
    $('#revidSelect').on('change', function () {
        $('#revidInput').val(this.value);
        get_diff_with_prev();
    });
    $('#diffTypeSelect').on('change', function () {
        get_diff_with_prev();
    });
    $("#history-unfiltered:empty").parent().hide();
    // comparison stuff
    display_comparison();
    $("#getComparisonButton").on('click', function () {display_comparison();})
});
///////////////////////


async function get_comparison(from, to) {
    let url = `https://en.wikipedia.org/w/rest.php/v1/revision/${from}/compare/${to}`;
    let json = await fetch(url).then(resp=>{ return resp.json(); }); 
    return json
}

async function display_comparison() {
    const fromRevid = $("#revidFromInput").val(),
        toRevid = $("#revidToInput").val();
    const json = await get_comparison(fromRevid, toRevid)

    renderjson.set_icons("⯈", "⯆")
        .set_show_to_level(2)
    $('#displayComparison').html(renderjson(json))
    if (json.diff) {
        const peek = peekHighlighted(json.diff);
        $('#peekComparison').html(renderjson(peek))
    } else {
        $('#peekComparison').html("(Error: comparison failed)")
    }
}

function onlyHighlightRanges(diff) {
    const mapped = diff.highlightRanges.map(hr => { 
        return {
            "type" : hr.type,
            "change" : substringByBytes(diff.text, hr.start, hr.length),
        }
    });
    return {
        "text": diff.text,
        "changes": mapped
    };
}
function substringByBytes(input, start, length) {
    const bytes = new TextEncoder().encode(input);
    return new TextDecoder().decode(bytes.subarray(start, start + length));
}
function peekHighlighted(diff) {
    let filtered = diff.filter(
        d => (d.type==3 || d.type==5) && d.highlightRanges.length > 0
    )
    return filtered.map(onlyHighlightRanges)
}




//////////////////////

function sync_all_displays() {
    // sync all the displayed items
    sync_regex();
    sync_historyURL();
    sync_diffURL();
}
function display_revisionsCount() {
    $('#revisionsCount').html(revisionsCount);
}
function lineBreakFix_infoMessages() {
    // Make URLs (class='info-message' elements) break nicely
    // (add breaker html after all '&' characters)
    const breakable = '<wbr>' // or '\u200B'
    $(".info-message").html(
        (_, html) => { return html.replace(/&amp;/g, '&amp;' + breakable) }
    );
}
function display_regex() {
    let re = new RegExp(
        pattern = $('#commentRegExp').val(),
        flags = $('#commentRegExpFlags').val())
    $('#displayRegExp').html(re)
    return re
}
function sync_regex() {
    display_regex();

    $('#commentRegExp,#commentRegExpFlags').on(
        'input',
        () => display_regex())
}
function get_baseURL() {
    const lang = $('#lang').val(),
        proj = $('#project').val(),
        tld = $('#tld').val()
    return `https://${lang}.${proj}.${tld}`
}
function get_historyURL() {
    const title = $('#pageTitle').val()
    return `${get_baseURL()}/w/rest.php/v1/page/${title}/history`
}
function setSelectedPage(p) {
    $('#pageTitle').val(p.key); // title is prettier but key is safer for urls
    $('#pageTitle').trigger('input'); // so, use key for everything
    $('#pageTitlePretty').html(p.title).attr('href', `${get_baseURL()}/wiki/${p.key}`);
    $('#pageDescription').html(p.description);
    $('#searchResultImg').attr('src', (p.thumbnail !== null) ? p.thumbnail.url : null);
}
async function search_titles(query, num_results = 5) {
    if (!query) return undefined;
    const searchURL = `${get_baseURL()}/w/rest.php/v1/search/title?`
    const url = searchURL + new URLSearchParams({ q: query, limit: num_results });
    // $('#queryURL').html(url); lineBreakFix_infoMessages();
    try {
        let json = await fetchJSON(url)
        let pages = json.pages

        $('#autocompleteSelect').html(
            // renderjson(pages.map((page) => {return page.title}))
            pages.map(function (page, i) {
                return $("<option />").val(i).text(page.title)
            })
        )
        return pages
    } catch (e) {
        console.error(e.message)
    }
}
function display_editCountURL() {
    const filterChoice = $('#filterChoice').val(),
        historyURL = `${get_historyURL()}/counts/`
    // initialize values
    let optionalParams = {}
    // TODO: in REST API, to and from must be used as a pair. see:
    //       https://www.mediawiki.org/wiki/API:REST_API/Reference#Response_schema
    //       this means to get edits 'olderThan' requires `from={earliestRevId}` and `to={olderThan}`.
    //       currently it's not worth it to get the earliest revid, so not implemented.
    // let olderThan = $('#olderThan').val()
    // if (olderThan != "" && olderThan != FINISHED_MESSAGE) optionalParams.to = olderThan
    type = (filterChoice !== "(none)") ? filterChoice : "edits"
    let url = historyURL + type
    if (Object.keys(optionalParams).length != 0) url += '?' + new URLSearchParams(optionalParams)
    $('#editCountURL').html(url)
    lineBreakFix_infoMessages();
    return (url)
}
function display_historyURL() {
    const filterChoice = $('#filterChoice').val(),
        historyURL = get_historyURL()
    // initialize values
    let optionalParams = {}
    let olderThan = $('#olderThan').val()
    if (olderThan != "" && olderThan != FINISHED_MESSAGE) optionalParams.older_than = olderThan
    if (filterChoice !== "(none)") optionalParams.filter = filterChoice
    let url = historyURL
    if (Object.keys(optionalParams).length != 0) url += '?' + new URLSearchParams(optionalParams)
    $('#historyURL').html(url)
    lineBreakFix_infoMessages();
    return (url)
}
function sync_historyURL() {
    display_historyURL();
    $('#lang,#proj,#tld,#pageTitle,#olderThan,#filterChoice').on(
        'input',
        () => {
            display_historyURL();
            display_editCountURL();
        })
}
function appendRevisionsToRevidSelect(revisions) {
    $.each(revisions, function () {
        revisionsCount++;
        let description = `${this.timestamp} ${this.id} ${(this.minor ? '(m)' : '')} ${this.comment}`
        $("#revidSelect").append(
            $("<option />").val(this.id).text(description.substring(0, MAX_STRING_LENGTH))
        );
    });
    display_revisionsCount(revisionsCount);
}
async function fetchJSON(url) {
    const rsp = await fetch(url, HEADERS),
        data = await rsp.json();
    return data;
}
async function get_history_count() {
    const url = display_editCountURL();
    try {
        let json = await fetchJSON(url);
        console.log("fetchJSON(editCountURL) = ", json);
        renderjson.set_show_to_level(0);
        // render everything as json
        $('#count').html(json.count)
        let numBatches = Math.ceil(json.count / 20)
        $('#countBatches').html(numBatches)
        $('#maxBatches').val(numBatches)
        $('#maxBatches').trigger('input')
        if (json.limit) {
            u = 'https://www.mediawiki.org/wiki/API:REST_API/Reference#Get_page_history_counts'
            $('#count').append(`warning! <a href="${u}">limit!</a> reached`)
        }

    } catch (e) {
        console.error(e.message)
    }
}
async function get_page_history() {
    let url = display_historyURL();
    let re = display_regex();
    let historyFiltered = [];
    const maxBatches = $('#maxBatches').val()
    for (let i = 0; i < maxBatches; i++) {
        try {
            let json = await fetchJSON(url);
            $('#historyBatch').html(i + 1)
            console.log(i, "fetchJSON(historyURL) = ", json);
            renderjson.set_show_to_level(0)
            $('#history-unfiltered').append(renderjson(json))
            revisions = $.grep(json.revisions, (o, i) => { return re.test(o.comment) })
            historyFiltered.push(...revisions)
            // whether to continue
            if (!json.hasOwnProperty('older')) {
                $('#olderThan').val(FINISHED_MESSAGE);
                $("#historyButton").attr("disabled", true);
                break
            }
            url = json.older
            $('#historyURL').html(url)
            $('#olderThan').val(url.split('=')[1])
            console.log(i, "setting historyURL to", url)
            $("#history-unfiltered").parent().show();
            numBatchesDisplayed = $("#history-unfiltered").find(".renderjson").length;
            $("#numBatchesDisplayed").html(numBatchesDisplayed)
        } catch (e) {
            console.error(e.message);
        }
    }
    console.log(historyFiltered);
    $('#history-filtered').append(renderjson(historyFiltered));
    appendRevisionsToRevidSelect(historyFiltered);
    // update the revidSelect and diffURL displays
    $('#revidInput').val($('#revidSelect').val())
    display_diffURL();
    get_diff_with_prev();
}
function display_diffURL() {
    const apiURL = `${get_baseURL()}/w/api.php`,
        revidInput = $('#revidInput').val(),
        diffType = $('#diffTypeSelect').val();
    const params = {
        action: "compare",
        format: "json",
        fromrev: revidInput,
        torelative: "prev",
        prop: "diff|ids|title|timestamp|comment|rel|diffsize|parsedcomment|size|user",
        difftype: diffType,
        origin: "*" // For nonauthenticated requests, this is fine.
    };
    const url = apiURL + '?' + new URLSearchParams(params);
    if (revidInput == "") {
        $('#diffURL').html("please supply revision id")
    } else {

        $('#diffURL').html(url)
        lineBreakFix_infoMessages();
    }
    return (url)
}
function sync_diffURL() {
    display_diffURL();
    $('#lang,#proj,#tld,#revidInput').on(
        'input',
        () => display_diffURL())
}
async function get_diff_with_prev() {
    const url = display_diffURL(),
        revid = $('#revidInput').val()
    console.log("get_diff_with_prev: ", revid)
    try {
        let json = await fetchJSON(url),
            compare = json.compare;
        console.log("fetchJSON(diffURL).compare = ", compare);
        renderjson.set_show_to_level(0);

        // link to view this revision on the wiki site
        $('#revLink')
            .attr('href', `${get_baseURL()}/w/index.php?diff=prev&oldid=${revid}`)
            .html(revid)
        // render everything as json
        $('#diffPrev-info').html(renderjson(compare))
        $("#revidFromInput").val(compare.fromrevid)
        $("#revidToInput").val(compare.torevid)
        display_comparison();

        // display the diff HTML
        let comment = $("<td />").attr('colspan', 4).addClass('diff-info')
            .html($.parseHTML(compare['toparsedcomment']))
        $('#diff-body').empty()
            .append($("<tr />").append(comment))
            .append($.parseHTML(compare['*']))
    } catch (e) {
        console.error(e.message)
    }
}
