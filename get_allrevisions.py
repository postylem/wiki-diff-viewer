import requests

S = requests.Session()

URL = "https://en.wikipedia.org/w/api.php"

PARAMS = {
    "action": "query",
    "arvprop": "ids|flags|timestamp|comment",
    "arvuser": "Jimpaz",
    "list": "allrevisions",
    "format": "json",
    "rvlimit": "100",
}

R = S.get(url=URL, params=PARAMS)
DATA = R.json()

ALLREVISIONS = DATA["query"]["allrevisions"]

for i,rev in enumerate(ALLREVISIONS):
    print(i)
    print(rev)