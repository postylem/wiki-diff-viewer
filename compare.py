import requests

S = requests.Session()

URL = "https://en.wikipedia.org/w/api.php"

def get_json_change(target_rev):
    params = {
        "action": "compare",
        "format": "json",
        "fromrev": target_rev,
        "torelative": "prev",
        "prop": "diff|rel|title|user|comment|timestamp",
        "difftype": "table",
    }
    response = S.get(url=URL, params=params).json()
    return response["compare"]

DATA = get_json_change("431840620")

for k in DATA:
    print(k)
    print(DATA[k])
