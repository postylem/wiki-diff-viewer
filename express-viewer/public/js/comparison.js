
$(function () {
    ['comparison'].forEach(label => {
        $(`#header-${label}`).on('click', function () { $(`#sec-${label}`).toggle() })
    });
    
    display_comparison();
    $("#getComparisonButton").on('click', function () {display_comparison();})
});


async function get_comparison(from, to) {
    let url = `/rest/revision/${from}/compare/${to}`;
    let json = await fetch(url).then(resp=>{ return resp.json(); }); 
    return json
}

async function display_comparison() {
    const fromRevid = $("#revidFromInput").val(),
        toRevid = $("#revidToInput").val();
    const json = await get_comparison(fromRevid, toRevid)

    renderjson.set_icons("⯈", "⯆")
        .set_show_to_level(2)
    $('#displayComparison').html(renderjson(json))
    const peek = peekHighlighted(json);

    $('#peekComparison').html(renderjson(peek))
}

function onlyHighlightRanges(diff) {
    const mapped = diff.highlightRanges.map(hr => { 
        return {
            "type" : hr.type,
            "change" : substringByBytes(diff.text, hr.start, hr.length),
        }
    });
    return {
        "text": diff.text,
        "changes": mapped
    };
}
function substringByBytes(input, start, length) {
    const bytes = new TextEncoder().encode(input);
    return new TextDecoder().decode(bytes.subarray(start, start + length));
}
function peekHighlighted(comparison_json) {
    let filtered = comparison_json.diff.filter(
        d => (d.type==3 || d.type==5) && d.highlightRanges.length > 0
    )
    return filtered.map(onlyHighlightRanges)
}