Real typos

Psychology:

1067538043


## JS API choices

There are three very similar APIs, it seems. 

1. Wikimedia (Core REST) API

2. MediaWiki (Action) API

3. MediaWiki REST API (maybe identical to 1?)


For example: to get the _history of the page "Earth" of English Wikipedia_, you can use it through wikimedia.org, or throught the project site:

-   Wikimedia.org **Core REST API** ([see docs](https://api.wikimedia.org/wiki/Core_REST_API))
    
    > <https://api.wikimedia.org/core/v1/wikipedia/en/page/Earth/history>

-   MediaWiki's **REST_API**, available at `rest.php` on the Wikipedia site ([see docs](https://www.mediawiki.org/wiki/API:REST_API))
    
    > <https://en.wikipedia.org/w/rest.php/v1/page/Earth/history>

**So, which to use?**

The MediaWiki Action API is most powerful, but the REST API seems more actively maintained, and the Wikimedia (REST) API [might be replaced with the MediaWiki REST API long term](https://stackoverflow.com/questions/74755359/mediawiki-rest-api-vs-mediawiki-action-api-vs-wikimedia-api-which-to-use-for)

## Using pywikibot

`conda activate wikiedit` then

```python
pip install "requests>=2.20.1" "mwparserfromhell>=0.5.0" "wikitextparser>=0.47.5"
pip install pywikibot
```

<https://public-paws.wmcloud.org/User:Jimpaz/test.ipynb>