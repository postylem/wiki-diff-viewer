// async function compare(fromrevid, torevid) {
//     // eg await compare(1157633768,1157658266)
//     const apiEndpoint = "https://en.wikipedia.org/w/rest.php/v1/"
//     const route = `revision/${fromrevid}/compare/${torevid}`;
//     const url = apiEndpoint + route;
//     const headers = {};
//     try {
//         let json = await fetch(url, headers).then(resp => { return resp.json(); });
//         return json
//     } catch (err) {
//         console.error(err.message)
//     }
// }



// Load Node modules
var express = require('express');
// const ejs = require('ejs');
// Initialise Express
var app = express();
// Render static files
app.use("/", express.static('public'));
// Set the view engine to ejs
app.set('view engine', 'ejs');


// *** GET Routes - display pages ***
// Root Route
var comparisonData = {diff: undefined}
app.get('/', async function (_req, res) {
    res.render('pages/index', {
        comparisonData: comparisonData
    });
});
app.get('/from/:from/to/:to', async function (req, res) {
    // eg: from/89691203/to/89738224
    // req.params; 
    // comparisonData = await compare(req.params.from, req.params.to)
    res.render('pages/comparison', {
        from: req.params.from,
        to: req.params.to,
        // comparisonData: comparisonData
    });
});

//////////////////////

const morgan = require("morgan");
const { createProxyMiddleware } = require("http-proxy-middleware");
require("dotenv").config();
// Configuration
const PORT = 3000;
const HOST = "localhost";
const API_BASE_URL = "https://en.wikipedia.org/";
// const { API_KEY_VALUE } = process.env; // if needed get api key from ./.env file
const API_REST_URL = `${API_BASE_URL}w/rest.php/v1`;

// Logging the requests
app.use(morgan("dev"));

// Proxy Logic : Proxy endpoints
app.use(
	"/rest/*",
	createProxyMiddleware({
		target: API_REST_URL,
		changeOrigin: true,
		pathRewrite: {
			"^/rest/": "",
		},
	})
);

// Starting our Proxy server
app.listen(PORT, HOST, () => {
	console.log(`Starting Proxy at ${HOST}:${PORT}`);
});

